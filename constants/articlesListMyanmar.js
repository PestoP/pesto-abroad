export default [
  {
    name: 'Hsipaw',
    componentName: 'hsipaw',
    linkTo: 'article',
    previousArticle: '',
    nextArticle: 'Loikaw',
    description: `Hsipaw se situe au nord de Mandalay. Cette destination vaut le détour grâce à ses treks dans une nature encore préservée 😍`
  },
  {
    name: 'Loikaw',
    componentName: 'loikaw',
    linkTo: 'article',
    previousArticle: 'Hsipaw',
    nextArticle: 'Yangon',
    description: `Loikaw est le point d'entrée pour rejoindre le peuple des Karens`
  },
  {
    name: 'Yangon',
    componentName: 'yangon',
    linkTo: 'article',
    previousArticle: 'Loikaw',
    nextArticle: 'Bagan',
    description: `Yangon est souvent un passage obligatoire grâce à son aéroport, autant en profiter pour découvrir cette ville très culturelle !`
  },
  {
    name: 'Bagan',
    componentName: 'bagan',
    linkTo: 'article',
    previousArticle: 'Yangon',
    nextArticle: 'Hpaan',
    description: `Bagan est un arrêt incontournable de Birmanie, ses temples aux pierres rouges sont magnifiques !`
  },
  {
    name: 'Hpaan',
    componentName: 'hpaan',
    linkTo: 'article',
    previousArticle: 'Bagan',
    nextArticle: 'Dawei',
    description: `Hpa-an est un super arrêt, les temples sont magnifiques et les points de vue top`
  },
  {
    name: 'Dawei',
    componentName: 'dawei',
    linkTo: 'article',
    previousArticle: 'Hpaan',
    nextArticle: '',
    description: `Dawei ou Tavoy, Un point d'entrée à des plages pour vous tout seul`
  },
]
