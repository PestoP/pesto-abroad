export default [
  {
    name: 'Pulau Weh',
    componentName: 'pulau-weh',
    nextArticle: 'Pangandaran',
    linkTo: 'article',
    description: `Pulau Weh est une île à l'extrémité de Sumatra. Et franchement, on a été conquis par son calme et sa biodiversité 😍`
  },
  {
    name: 'Pangandaran',
    componentName: 'pangandaran',
    previousArticle: 'Pulau Weh',
    nextArticle: 'Prambanan',
    linkTo: 'article',
    description: `Entre Jakarta et Yogyakarta, Pangandaran permet un stop très agréable où surf, canyoning et détente sont au rendez-vous...`
  },
  {
    name: 'Prambanan',
    componentName: 'prambanan',
    previousArticle: 'Pangandaran',
    nextArticle: 'Borobudur',
    linkTo: 'article',
    description: `Un temple qui vaut le détour, bien que très cher par rapport au coût de la vie indonnésienne. Il rappelle un peu Angkor Wat au Cambodge`
  },
  {
    name: 'Borobudur',
    componentName: 'borobudur',
    previousArticle: 'Prambanan',
    nextArticle: 'Yogyakarta',
    linkTo: 'article',
    description: `Un temple pas comme les autres, il ne ressemble à aucun autre temple que j'ai pu visiter en Asie du sud-est ! Il vaut le détour, cependant peut-être pas pour le lever du soleil...`
  },
  {
    name: 'Yogyakarta',
    componentName: 'yogyakarta',
    previousArticle: 'Borobudur',
    nextArticle: 'Mont Bromo',
    linkTo: 'article',
    description: `Une ville qui n'a pas grand intérêt, si ce n'est son emplacement proche des temples Borobudur et Prambanan`
  },
  {
    name: 'Mont Bromo',
    componentName: 'mont-bromo',
    previousArticle: 'Yogyakarta',
    nextArticle: 'Mont Kawah Ijen',
    linkTo: 'article',
    description: `Une des plus belles merveilles que j'ai rencontré en Asie du sud-est, un immanquable si vous allez sur l'île de Java !`
  },
  {
    name: 'Mont Kawah Ijen',
    componentName: 'ijen',
    previousArticle: 'Mont Bromo',
    nextArticle: 'Pemuteran',
    linkTo: 'article',
    description: `L'ascension n'est pas extraordinaire mais ça en vaut la chandelle, 'Blue fire' puis un lever de soleil sur le lac acide le plus grand du monde...`
  },
  {
    name: 'Pemuteran',
    componentName: 'pemuteran',
    previousArticle: 'Mont Kawah Ijen',
    nextArticle: 'Munduk',
    linkTo: 'article',
    description: `Un village qui ressemble à une longue rue au bord de la route principale. Il réserve de la Tranquillité et du snorkeling`
  },
  {
    name: 'Munduk',
    componentName: 'munduk',
    previousArticle: 'Pemuteran',
    nextArticle: 'Ubud',
    linkTo: 'article',
    description: `Étape à l'écart de la foule de Bali. Des jolies cascades bien qu'elles soient payantes, mais c'est surtout la porte d'accès à un circuit allant jusqu'au rizières de Jatiluwih...`
  },
  {
    name: 'Ubud',
    componentName: 'ubud',
    previousArticle: 'Munduk',
    nextArticle: 'Amed',
    linkTo: 'article',
    description: `La foule y est présente car c'est un endroit stratégique de Bali, beaucoup de beau temples son aux alentours, une très belle campagne. On peut aussi y faire un cours de cuisine Balinaise.`
  },
  {
    name: 'Amed',
    componentName: 'amed',
    previousArticle: 'Ubud',
    nextArticle: 'Nusa Penida',
    linkTo: 'article',
    description: `Plage, détente, yoga, ambiance reggea... Cet endroit est aussi reconnue pour ses 3 spots extraordinaires de snorkeling !`
  },
  {
    name: 'Nusa Penida',
    componentName: 'nusa-penida',
    previousArticle: 'Amed',
    nextArticle: 'Le sud de Bali',
    linkTo: 'article',
    description: `Île encore un peu sauvage tout près de Bali, d'innombrables spots à voir. Un scooter et la liberté est dans vos mains, MA-GNI-FI-QUE !`
  },
  {
    name: 'Le sud de Bali',
    componentName: 'sud-de-bali',
    previousArticle: 'Nusa Penida',
    nextArticle: 'Les îles Gili',
    linkTo: 'article',
    description: `Uluwatu, Jimbaran, Kuta, etc. Un repère de surfeur et de gens qui veulent faire des soirées ou des achats. De temps en temps, ça fait du bien.`
  },
  {
    name: 'Les îles Gili',
    componentName: 'gili',
    previousArticle: 'Le sud de Bali',
    nextArticle: 'Mont Egon',
    linkTo: 'article',
    description: `Les îles Gili nous ont permis de bien nous détendre mais aussi d'observer de magnifiques tortues. On vous explique tout dans l'article 🙂`
  },
  {
    name: 'Mont Egon',
    componentName: 'mont-egon',
    previousArticle: 'Les îles Gili',
    nextArticle: `La croisière du Komodo`,
    linkTo: 'article',
    description: `Ce volcan qui, de part sa basse fréquentation, est d'un calme et d'une beauté à couper le souffle ! Un peu physique, mais ça vaut le coup à l'arrivée.`
  },
  {
    name: `La croisière du Komodo`,
    componentName: 'croisiere-komodo',
    previousArticle: 'Mont Egon',
    linkTo: 'article',
    description: `Cette croisière de 3 jours permet de voir des paysages magnifiques, faire du snorkeling et admirer une multitude de couchers de soleil. Elle permet en plus de rejoindre Lombok depuis Labuang Bajo 😉`
  }
]
