
export default {
  target: 'static',
  /*
  ** Headers of the page
  */
  head: {
    title: `Blog d'aventures | Bob & Pesto`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: `Blog d'un couple de voyageurs voulant partager leurs expériences autour du monde 🙂` }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
    htmlAttrs: {
      lang: 'fr',
      dir: 'ltr',
    },
  },
  /*
  ** Generate prod
  */
  generate: {
    routes: [
      '/a-propos',
      '/pays/cambodge',
      '/pays/myanmar',
      '/pays/laos',
      '/pays/malaisie',
      '/pays/philippines',
      '/pays/thailande',
      '/pays/vietnam',
      '/pays/indonesie',
      '/pays/indonesie/articles/gili',
      '/pays/indonesie/articles/pulau-weh',
      '/pays/indonesie/articles/pangandaran',
      '/pays/indonesie/articles/prambanan',
      '/pays/indonesie/articles/borobudur',
      '/pays/indonesie/articles/yogyakarta',
      '/pays/indonesie/articles/mont-bromo',
      '/pays/indonesie/articles/pemuteran',
      '/pays/indonesie/articles/munduk',
      '/pays/indonesie/articles/ubud',
      '/pays/indonesie/articles/amed',
      '/pays/indonesie/articles/nusa-penida',
      '/pays/indonesie/articles/sud-de-bali',
      '/pays/indonesie/articles/croisiere-komodo',
      '/pays/indonesie/articles/mont-egon',
      '/pays/myanmar/articles/hsipaw',
      '/pays/myanmar/articles/yangon',
      '/pays/myanmar/articles/loikaw',
      '/pays/myanmar/articles/bagan',
      '/pays/myanmar/articles/hpaan',
      '/pays/myanmar/articles/dawei',
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/style/normalize.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/metadata',
    '~/plugins/structuredData',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/google-fonts',
  ],
  googleFonts: {
    families: {
      Poppins: {
        wght: [
          300,
          400,
          500,
          700,
          900,
        ],
      },
      K2D: {
        wght: [
          400,
          700,
        ],
      },
      Dosis: {
        wght: [
          200,
        ],
      }
    },
    display: 'swap',
    download: true,
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    '@nuxtjs/google-gtag',
  ],
  'google-gtag': {
    id: process.env.GOOGLE_ANALYTICS_ID,
    config: {
      anonymize_ip: true, // anonymize IP 
      send_page_view: false, // might be necessary to avoid duplicated page track on page reload
    }
  },
  robots: {
    UserAgent: '*',
    Disallow: '/user',
  },
  sitemap: {
    path: '/sitemap.xml',
    cacheTime: 1000 * 60 * 60 * 2,
    trailingSlash: true,
    gzip: true,
    hostname: 'https://bob-pesto.com',
    defaults: {
      changefreq: 'monthly',
      priority: 1,
      lastmod: new Date()
    },
    routes: [
      {
        url: '/',
        changefreq: 'monthly',
        priority: 0.6,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/a-propos',
        changefreq: 'yearly',
        priority: 0.6,
        lastmod: '2021-12-01T13:30:00.000Z'
      },
      {
        url: '/pays',
        changefreq: 'monthly',
        priority: 0.8,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/laos',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/malaisie',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/myanmar',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/cambodge',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/vietnam',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/philippines',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/thailande',
        changefreq: 'monthly',
        priority: 0.2,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie',
        changefreq: 'monthly',
        priority: 0.9,
        lastmod: '2021-04-10T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/gili',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/pulau-weh',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/pangandaran',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/prambanan',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/borobudur',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/yogyakarta',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/mont-bromo',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/ijen',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/pemuteran',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/munduk',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/ubud',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/amed',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/nusa-penida',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: '/pays/indonesie/articles/sud-de-bali',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2020-12-13T13:30:00.000Z'
      },
      {
        url: 'pays/indonesie/articles/croisiere-komodo',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2021-04-10T13:30:00.000Z'
      },
      {
        url: 'pays/indonesie/articles/mont-egon',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2021-04-10T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/hsipaw',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-03-06T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/yangon',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/loikaw',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/bagan',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/hpaan',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
      {
        url: 'pays/myanmar/articles/dawei',
        changefreq: 'monthly',
        priority: 1,
        lastmod: '2022-07-20T13:30:00.000Z'
      },
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  router: {
    scrollBehavior () {
      /* WOW, great... Clap Clap, Something is broken with the scroll
        It doesn't go to the top after a big scroll and a clic to another page (in Firefox, didn't try in Chrome or something else)
        Here is a dirty hack but something is wrong with the page's height. Hope an intern will fix that someday :)
      */
      return { x: 0, y: -9999999 }
    }
  }
}
