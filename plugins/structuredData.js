export default (context, inject) => {
  const addStructuredData = (data) => (
    [
      {
        type: 'application/ld+json',
        json: {
          '@context': 'https://schema.org',
          '@type': 'Article',
          'headline': data.title,
          'description': data.description,
          'thumbnailUrl': `https://res.cloudinary.com/djchx0gtn/image/upload/v1642442844/structured_data/${data.src}_16x9.jpg`,
          'genre': 'blog voyage',
          'keywords': data.keywords,
          'url': 'https://bob-pesto.com/' + data.url,
          "image": [
            `https://res.cloudinary.com/djchx0gtn/image/upload/v1642442844/structured_data/${data.src}_1x1.jpg`,
            `https://res.cloudinary.com/djchx0gtn/image/upload/v1642442844/structured_data/${data.src}_4x3.jpg`,
            `https://res.cloudinary.com/djchx0gtn/image/upload/v1642442844/structured_data/${data.src}_16x9.jpg`
            ],
          "datePublished": data.datePublished,
          "dateModified": data.dateModified,
          "author": {
            "@type": "Person",
            "name": "Bob Pesto",
            "url": "https://bob-pesto.com/a-propos"
          } 
        }
      }
    ]
  )
  // Now I can use $addStructuredData in Vue, context and store
  inject('addStructuredData', addStructuredData)
}