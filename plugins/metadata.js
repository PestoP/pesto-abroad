export default (context, inject) => {
  const addMetaData = (data, isNotArticle) => {
    const commonMetaData = [
      { hid: 'og:title', property: 'og:title', content: data.title },
      { hid: 'description', name: 'description', content: data.description },
      { hid: 'og:description', property: 'og:description', content: data.description },
      { hid: 'og:locale', property: 'og:locale', content: 'fr_FR' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Bob & Pesto' },
      { hid: 'og:image', property: 'og:image', content: `https://res.cloudinary.com/djchx0gtn/image/upload/v1642442844/open_graph/${data.image.src}_open_graph.jpg` },
      { hid: 'og:image:alt', property: 'og:image:alt', content: data.image.alt },
      { hid: 'og:image:height', property: 'og:image:height', content: data.image.height || 680 },
      { hid: 'og:image:width', property: 'og:image:width', content: data.image.width || 1300 },
      { hid: 'og:url', property: 'og:url', content: 'https://bob-pesto.com/' + data.url },
      { hid: 'og:type', property: 'og:type', content: data.type },
    ]
    
    return isNotArticle ? 
      commonMetaData
      : 
      [
        ...commonMetaData,
        ...[
          { hid: 'article:published_time', property: 'article:published_time', content: data.publishedTime},
          { hid: 'article:modified_time', property: 'article:modified_time', content: data.modifiedTime},
          { hid: 'article:section', property: 'article:section', content: 'Travel Blog'},
        ]
      ]
  }
  // Now I can use $addMetaData in Vue, context and store
  inject('addMetaData', addMetaData)
}